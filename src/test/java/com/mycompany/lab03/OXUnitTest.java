/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Acer
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }

   
    
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    //TDD Test Driven Development
    @Test
    public void testCheckWinRow2_o_output_true() {
        String[][] table = {{"-", "-", "-"}, {"o", "o", "o"}, {"-", "-", "-"}};
        String currentplayer = "o";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow1_o_output_true() {
        String[][] table = {{"o", "o", "o"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentplayer = "o";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow3_o_output_true() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"o", "o", "o"}};
        String currentplayer = "o";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol1_o_output_true() {
        String[][] table = {{"o", "-", "-"}, {"o", "-", "-"}, {"o", "-", "-"}};
        String currentplayer = "o";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol2_o_output_true() {
        String[][] table = {{"-", "o", "-"}, {"-", "o", "-"}, {"-", "o", "-"}};
        String currentplayer = "o";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol3_o_output_true() {
        String[][] table = {{"-", "-", "o"}, {"-", "-", "o"}, {"-", "-", "o"}};
        String currentplayer = "o";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow2_x_output_true() {
        String[][] table = {{"-", "-", "-"}, {"x", "x", "x"}, {"-", "-", "-"}};
        String currentplayer = "x";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow1_x_output_true() {
        String[][] table = {{"x", "x", "x"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentplayer = "x";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow3_x_output_true() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"x", "x", "x"}};
        String currentplayer = "x";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol1_x_output_true() {
        String[][] table = {{"x", "-", "-"}, {"x", "-", "-"}, {"x", "-", "-"}};
        String currentplayer = "x";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol2_x_output_true() {
        String[][] table = {{"-", "x", "-"}, {"-", "x", "-"}, {"-", "x", "-"}};
        String currentplayer = "x";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol3_x_output_true() {
        String[][] table = {{"-", "-", "x"}, {"-", "-", "x"}, {"-", "-", "x"}};
        String currentplayer = "x";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinCol3_x_output_false1() {
        String[][] table = {{"-", "-", "o"}, {"-", "-", "x"}, {"-", "-", "x"}};
        String currentplayer = "x";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(false, result);
    } 
    @Test
    public void testCheckWinCol3_x_output_false2() {
        String[][] table = {{"-", "-", "x"}, {"-", "-", "o"}, {"-", "-", "x"}};
        String currentplayer = "x";
        boolean result = OX.checkWin(table, currentplayer);
        assertEquals(false, result);
    }    
}
