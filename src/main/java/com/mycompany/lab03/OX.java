/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab03;

/**
 *
 * @author Acer
 */
class OX {

    static boolean checkWin(String[][] table, String currentplayer) {
        if (checkRow(table, currentplayer)) {
            return true;
        }
        if (checkCol(table, currentplayer)) {
            return true;
        }
        if (checkA(table, currentplayer)) {
            return true;
        }
        if (checkB(table, currentplayer)) {
            return true;
        }

        return false;
    }

    private static boolean checkRow(String[][] table, String currentplayer) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(table, currentplayer, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentplayer, int row) {
        return table[row][0].equals(currentplayer) && table[row][1].equals(currentplayer) && table[row][2].equals(currentplayer);
    }

    private static boolean checkCol(String[][] table, String currentplayer) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(table, currentplayer, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(String[][] table, String currentplayer, int col) {
        return table[0][col].equals(currentplayer) && table[1][col].equals(currentplayer) && table[2][col].equals(currentplayer);
    }

    private static boolean checkA(String[][] table, String currentplayer) {
        return table[0][0].equals(currentplayer) && table[1][1].equals(currentplayer) && table[2][2].equals(currentplayer);
    }

    private static boolean checkB(String[][] table, String currentplayer) {
        return table[2][0].equals(currentplayer) && table[1][1].equals(currentplayer) && table[0][2].equals(currentplayer);
    }
}
